# My config for Emacs' DOOM

## DOOM Installation

```sh
git clone https://github.com/hlissner/doom-emacs ~/.emacs.d

export PATH="$HOME/.emacs.d/bin:$PATH"

doom install  # will ask questions !

# Choose own terminal shell
echo "export PATH=\"\$HOME/.emacs.d/bin:\$PATH\"" >> .bashrc
echo "export PATH=\"\$HOME/.emacs.d/bin:\$PATH\"" >> .zshrc
```

## Config Installation

```sh
git clone https://gitlab.com/isushik94/emacs-doom.git ~/.doom.d

doom sync

doom env
```

## Issues

If eglot + flymake produces freezes:

    https://github.com/hlissner/doom-emacs/issues/5644

